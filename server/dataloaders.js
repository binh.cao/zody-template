import Dataloader from 'dataloader'
import User from './features/user/model'

async function batchUsers(keys) {
  return await User.find({_id: {$in: keys}}).toArray()
}

module.exports = () => ({
  userLoader: new Dataloader(
    keys => batchUsers(keys),
    {cacheKeyFn: key => key.toString()}
  )
})