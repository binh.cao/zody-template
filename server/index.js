import {authenticate} from './authentication'
import {graphqlExpress, graphiqlExpress} from 'apollo-server-express'
import bodyParser from 'body-parser'
import buildDataLoaders from './dataloaders'
import dbConnector from './mongo-connector'
import express from 'express'
import formatError from './formatError'
import morgan from 'morgan'
import schema from './schema.js'

async function start() {
  await dbConnector.connect()
  const app = express()
  const buildOptions = async (req, res) => {
    // const user = await authenticate(req)
    return {
      context: {
        // dataloaders: buildDataLoaders(mongo),
        // mongo,
        user: ''
      },
      formatError,
      schema
    }
  }
  app.use(morgan('dev'))
  app.use('/graphql', bodyParser.json(), graphqlExpress(buildOptions))
  app.use('/graphiql', graphiqlExpress({
    endpointURL: '/graphql',
    passHeader: `'Authorization': 'bearer token-binh@mail.com'`
  }))
  require('./init')
  const PORT = 3000
  app.listen(PORT, () => {
    console.log(`Server running on port ${PORT}.`)
  })
}

start()
