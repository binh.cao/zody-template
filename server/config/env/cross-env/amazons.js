const thumbnailPrefix = 'sm_'
const mediumPrefix = 'md_'

export default {
  amazon: {
    name: {
      sticker: 'stickers.zip',
      defaultPhoto: 'default-photo.jpg',
      defaultAvatar: 'default-avatar.jpg',
      defaultLogo: 'default-logo.png'
    },
    prefix: {
      avatar: 'avatars/',
      business: 'businesses/',
      chat: 'chats/',
      cover: 'covers/',
      menu: 'menus/',
      sticker: 'stickers/',
      dish: 'dishes/'
    },
    thumbnailPrefix,
    mediumPrefix
  }
}
