export default {
	// Strict data
	strict: {
		hexaColorPattern: /^#[0-9a-f]{6}$/i,
		objectId: /^[0-9a-fA-F]{24}$/,
		price: /^[1-9][0-9]*$/,
		phone: /^\+?1?(\d{10,12}$)/,
		verifyPhone: {
			codeLength: 6
		}
	}
}
