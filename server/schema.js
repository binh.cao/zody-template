const {makeExecutableSchema} = require('graphql-tools')
import UserSchema from './features/user/schema'
import UserResolver from './features/user/resolvers'

let typeDefs = `
  ${UserSchema.types}
  
  type Mutation {
    ${UserSchema.mutations}
  }
  
  type Query {
    ${UserSchema.queries}
  }
`

let queries = Object.assign({}, UserResolver.queries)
let mutations = Object.assign({}, UserResolver.mutations)
let data = Object.assign({}, UserResolver.data)

let resolvers = {
  Query: queries,
  Mutation: mutations
}

resolvers = Object.assign(resolvers, data)

module.exports = makeExecutableSchema({typeDefs, resolvers})