/**
 * Get message from error object
 * @param {Object} error: error information
 * @return {String}
 */
import commonCode from '../locales/common'

function getErrorMessage(error) {
	console.log('getErrorMessage', error)
	if (!error) {
		return {
			code: commonCode.serverError.code
		}
	}

	let code = ''
	if (error.name === 'MongoError') {
		if (error.code === 11000) {
			code = commonCode.dataAlreadyExisted.code
		} else {
			code = commonCode.serverError.code
		}
	} else if (error.errors) {
		let key = Object.keys(error.errors)[0]
		code = error.errors[key] ? error.errors[key].message : commonCode.serverError.code
	} else if (error.code) {
		return error
	} else {
		code = -1
	}
	return {
		code: code,
		message: error.message
	}
}

function getErrorMessageWithPrefix(error, prefix = '') {
	console.log('getErrorMessageWithPrefix', error)
	if (!error) {
		return {
			code: commonCode.serverError.code
		}
	}

	let code = '', message = ''
	if (error.name === 'MongoError') {
		if (error.code === 11000) {
			code = commonCode.dataAlreadyExisted.code
		} else {
			code = commonCode.serverError.code
		}
	} else if (error.name === 'ValidationError') {
		let key = Object.keys(error.errors)[0]
		code = commonCode.idNotExists.code
		message = `${key} ${commonCode.idNotExists.message}`
	} else if (error.errors) {
		let key = Object.keys(error.errors)[0]
		message = error.errors[key] ? error.errors[key].message : commonCode.serverError.message
		code = validationError[prefix][key].code
	} else {
		code = -1
		message = error.message
	}

	return {
		code: code,
		message: message
	}
}

// Export
export {
	getErrorMessage,
	getErrorMessageWithPrefix
}
