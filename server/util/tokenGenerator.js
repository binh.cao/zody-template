import {sign} from 'jsonwebtoken'
import config from '../config'

/**
 * @param  {Object} data: payload data
 * @return {String}
 */
function generate(data) {
  // Sending the payload inside the token, expire in 30 days
  return sign(data, config.secret, {expiresIn: '30d'})
}

export default {
  generate
}
