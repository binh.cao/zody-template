import config from '../config'

/**
 * Return default cover
 * @return {Object}
 */
function defaultPhoto() {
  return config.S3.host + config.amazon.name.defaultPhoto
}

/**
 * Return default avatar
 * @return {String}
 */
function defaultAvatar() {
  return config.S3.host + config.amazon.name.defaultAvatar
}

/**
 * Return default logo
 * @return {String}
 */
function defaultLogo() {
  return config.S3.host + config.amazon.name.defaultLogo
}

/**
 * Avatar url
 *
 * @param  {String} name
 * @return {String}
 */
function getAvatarUrl(name) {
  return name ? (config.S3.host + config.amazon.thumbnailPrefix + name) : defaultAvatar()
}

/**
 * Photo url
 *
 * @param  {String} name
 * @return {String}
 */
function getPhotoUrl(name) {
  return name ? (config.S3.host + config.amazon.thumbnailPrefix + name) : defaultPhoto()
}

/**
 * Get business cover url
 *
 * @param  {String} name
 * @return {String}
 */
function getBusinessCoverUrl(name) {
  return name ? (config.S3.host + config.amazon.mediumPrefix + name) : defaultPhoto()
}

/**
 * Photo url
 *
 * @param  {String} name
 * @return {String}
 */
function getLogoUrl(name) {
  return name ? (config.S3.host + config.amazon.thumbnailPrefix + name) : defaultLogo()
}

function defaultUser() {
  return {
    _id: '',
    avatar: defaultAvatar(),
    name: 'Anonymous'
  }
}

export default {
  defaultPhoto,
  defaultAvatar,
  defaultLogo,
  defaultUser,
  getPhotoUrl,
  getAvatarUrl,
  getBusinessCoverUrl,
  getLogoUrl
}
