function formatPhoneNumber(phone = '') {
  if (!phone) {
    return ''
  }

  // Cast to string
  phone = phone.toString()

  // Remove all space character in phone number
  phone = phone.split(' ').join('')

  if (phone[0] !== '0' && (phone[0] !== '8' && phone[1] !== '4') && (phone.indexOf('+84') === -1)) {
    phone = '0' + phone
  }

  // If format is 0xxx xxx xxx, change to +84 xxx xxx xxx
  if (phone[0] === '0') {
    phone = '+84' + phone.substring(1)
  }
  // If format is 84 xxx xxx xxx, add '+' to the first of phone number
  else if (phone[0] === '8' && phone[1] === '4') {
    phone = '+' + phone
  }
  return phone
}

export default {
  formatPhoneNumber
}