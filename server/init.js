import config from './config/env/index'
require('colors')

function start() {
  ['log', 'warn'].forEach((method) => {
    let old = console[method]
    console[method] = function () {
      let stack = (new Error()).stack.split(/\n/)
      if (stack[0].indexOf('Error') === 0) {
        stack = stack.slice(1)
      }
      let args = [].slice.apply(arguments).concat([stack[1].trim()])
      return old.apply(console, args)
    }
  })

  if (process.env.NODE_ENV === config.env.production) {
    global.adminId = process.env.ADMIN_ID
  } else {
    global.adminId = '573fe5f870a07c52085dba15'
  }
  if (!global.adminId) {
    console.log('*** Please setup a SUPPORT ACCOUNT before run server'.red)
    process.exit(1)
  }
}

start()
