/**
 * User code, from 601-700
 */
import UserConfig from '../features/user/config'

export default {
	nameInvalid: {
		code: 600,
		message: 'Tên hiển thị không được chứa ký tự đặc biệt.'
	},
	nameRequired: {
		code: 601,
		message: 'Tên hiển thị không được trống'
	},
	nameMinLength: {
		code: 602,
		message: `Tên hiển thị chỉ được tối đa ${UserConfig.strict.nameMinLength} ký tự` // 3
	},
	nameMaxLength: {
		code: 603,
		message: `Tên hiển thị chỉ được tối đa ${UserConfig.strict.nameMaxLength} ký tự` // 32
	},
	invalidEmailFormat: {
		code: 604,
		message: 'Email không đúng định dạng'
	},
	emailAlreadyInUse: {
		code: 605,
		message: 'Email này đã được sử dụng bởi tài khoản khác'
	},
	invalidPasswordLength: {
		code: 606,
		message: `Mật khẩu phải ít nhất ${UserConfig.strict.pwdMinLength} ký tự` // 6
	},
	phoneNumberAlreadyInUse: {
		code: 607,
		message: 'Số điện thoại này đã được sử dụng bởi tài khoản khác'
	},
	invalidPhoneNumber: {
		code: 608,
		message: 'Số điện thoại không đúng định dạng'
	},
	loginFacebookError: {
		code: 609,
		message: 'Đã xảy ra lỗi khi đăng nhập với Facebook'
	},
	facebookEmailUnverified: {
		code: 610,
		message: 'Email tài khoản của bạn chưa được Facebook xác thực, vui lòng xác thực lại để hoàn tất đăng ký'
	},
	invalidReferralCode: {
		code: 611,
		message: 'Mã mời bạn không hợp lệ, vui lòng kiểm tra lại'
	},
	userAlreadyInputReferralCode: {
		code: 613,
		message: 'Bạn đã nhập mã mời bạn rồi'
	},
	phoneRequired: {
		code: 614,
		message: 'Số điện thoại không được trống'
	},
	invalidLocale: {
		code: 615,
		message: 'Lựa chọn ngôn ngữ không hợp lệ'
	},
	invalidFBToken: {
		code: 616,
		message: 'Xác thực Facebook thất bại, vui lòng thử lại'
	},
	wrongOldPassword: {
		code: 617,
		message: 'Mật khẩu cũ không chính xác, vui lòng thử lại'
	},
	reachLimitSMS: {
		code: 618,
		message: `Bạn chỉ có thể gửi tối đa ${ UserConfig.maxVerifySMS } tin nhắn xác nhận 1 ngày` // 3
	},
	userAlreadyVerified: {
		code: 619,
		message: 'Tài khoản này đã được xác nhận trước đây'
	},
	alreadyReceivedDailyBonus: {
		code: 620,
		message: 'Bạn đã nhận điểm thưởng hôm nay rồi'
	},
	referralCodeExisted: {
		code: 621,
		message: 'Mã mời bạn này đã được sử dụng bởi người dùng khác, vui lòng nhập mã khác'
	},
	phoneNotConfirmed: {
		code: 623,
		message: 'Tài khoản chưa được xác nhận số điện thoại'
	},
	cannotChangePasswordWithFBAccount: {
		code: 624,
		message: 'Tài khoản đăng nhập bằng Facebook không thể đổi mật khẩu'
	},
	cannotChangePasswordWithLoginPhoneNumber: {
		code: 625,
		message: 'Tài khoản đăng nhập bằng số điện thoại không thể đổi mật khẩu'
	},
	notAdmin: {
		code: 626,
		message: 'Tài khoản này không phải admin'
	}
}
