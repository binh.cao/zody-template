/**
 * Common code, from 1-999
 */

export default {
  requireLogin: {
    code: 1,
    message: 'Bạn phải đăng nhập để thực hiện hành động này'
  },
  noPermission: {
    code: 2,
    message: 'Bạn không có quyền thực hiện hành động này'
  },
  authenticateFailed: {
    code: 3,
    message: 'Tên đăng nhập hoặc mật khẩu không đúng, vui lòng kiểm tra lại'
  },
  tokenVerifyFailed: {
    code: 4,
    message: 'Phiên đăng nhập đã hết hạn, vui lòng đăng nhập lại'
  },
  unauthorizedError: {
    code: 5,
    message: 'Đã xảy ra lỗi, vui lòng thử lại'
  },
  apiNotFound: {
    code: 6,
    message: 'API không tìm thấy'
  },
  serverError: {
    code: 7,
    message: 'Đã xảy ra lỗi, vui lòng thử lại'
  },
  successfully: {
    code: 8,
    message: 'Thành công'
  },
  dataAlreadyExisted: {
    code: 9,
    message: 'Dữ liệu này đã tồn tại trong hệ thống'
  },
  validateRequestDataError: {
    code: 10,
    message: 'Dữ liệu không đúng định dạng'
  },
  invalidMailTemplate: {
    code: 11,
    message: 'Định dạng mail để gửi không tìm thấy'
  },
  sendMailError: {
    code: 12,
    message: 'Đã xảy ra lỗi khi gửi email, vui lòng liên hệ "Zody - Hỗ trợ" để được hỗ trợ'
  },
  phoneOrEmailIsRequired: {
    code: 13,
    message: 'Số điện thoại hoặc mật khẩu không được trống'
  },
  fbAccountCannotForgotPassword: {
    code: 14,
    message: 'Tài khoản đăng nhập bằng Facebook không thể sử dụng chức năng quên mật khẩu'
  },
  expiredForgotPasswordToken: {
    code: 15,
    message: 'Liên kết không tồn tại hoặc đã hết hạn'
  },
  sendSMSError: {
    code: 16,
    message: 'Đã xảy ra lỗi khi gửi tin nhắn, vui lòng liên hệ "Zody - Hỗ trợ" để được hỗ trợ'
  },
  registerNotSupported: {
    code: 17,
    message: 'Đăng ký bằng email không còn được hỗ trợ bởi Zody, vui lòng nâng cấp lên phiên bản mới nhất để tiếp tục sử dụng'
  },
  userNotFound: {
    code: 50,
    message: 'Người dùng không tìm thấy'
  },
  businessNotFound: {
    code: 51,
    message: 'Địa điểm không tìm thấy'
  },
  categoryNotFound: {
    code: 52,
    message: 'Danh mục không tìm thấy'
  },
  cuisineNotFound: {
    code: 53,
    message: 'Ẩm thực không tìm thấy'
  },
  purposeNotFound: {
    code: 54,
    message: 'Mục đích không tìm thấy'
  },
  beaconNotFound: {
    code: 55,
    message: 'Beacon không tìm thấy'
  },
  menuNotFound: {
    code: 56,
    message: 'Menu không tìm thấy'
  },
  dishNotFound: {
    code: 57,
    message: 'Món ăn không tìm thấy'
  },
  photoNotFound: {
    code: 58,
    message: 'Hình ảnh không tìm thấy'
  },
  chainNotFound: {
    code: 59,
    message: 'Chuỗi cửa hàng không tìm thấy'
  },
  eventNotFound: {
    code: 60,
    message: 'Sự kiện không tìm thấy'
  },
	voucherNotFound: {
		code: 61,
		message: 'Voucher không tìm thấy'
	},
	voucherGroupNotFound: {
		code: 61,
		message: 'Voucher Group không tìm thấy'
	},
  spinNotFound: {
    code: 62,
    message: 'Spin game không tìm thấy'
  },
  prizeNotFound: {
    code: 63,
    message: 'Phần thưởng quay số không tìm thấy'
  },
  luckyCheckinNotFound: {
    code: 64,
    message: 'Checkin may mắn không tìm thấy'
  },
  questNotFound: {
    code: 65,
    message: 'Nhiệm vụ không tìm thấy'
  },
  roomNotFound: {
    code: 66,
    message: 'Room không tìm thấy'
  },
  rewardNotFound: {
    code: 67,
    message: 'Phần thưởng không tìm thấy'
  },
  bannerNotFound: {
    code: 68,
    message: 'Banner không tìm thấy'
  },
  collectionNotFound: {
    code: 69,
    message: 'Collection không tìm thấy'
  },
  billNotFound: {
    code: 70,
    message: 'Hoá đơn không tìm thấy'
  },
  campaignNotFound: {
    code: 71,
    message: 'Chiến dịch không tìm thấy'
  },
  luckyDrawNotFound: {
    code: 72,
    message: 'LuckyDraw không tìm thấy hoặc đã kết thúc'
  },
  notEnoughTurnForLuckyDraw: {
    code: 73,
    message: 'Bạn đã hết lượt quay, tích điểm thêm để có lượt quay và nhận những phần quà hấp dẫn nha.'
  },
  prizeCategoryNotFound: {
    code: 74,
    message: 'prizeCategory không tìm thấy'
  },
	cannotEditPrize: {
		code: 75,
		message: 'Không được sửa prize mặc định'
	},
  promotionCodeNotFound: {
    code: 76,
    message: 'Mã giảm giá không tìm thấy hoặc đã hết hạn'
  },
  brandNotFound: {
    code: 77,
    message: 'Thương hiệu không tìm thấy'
  },
	targetNotFound: {
		code: 78,
		message: 'target không tìm thấy'
	},
  chatGlobalNotFound: {
    code: 79,
    message: 'Tin nhắn gửi không tìm thấy hoặc đã hết hạn'
  },
  chatGuideNotFound: {
    code: 80,
    message: 'Chat guide không tìm thấy'
  },
	ratingNotFound: {
		code: 81,
		message: 'Đánh giá không tìm thấy'
  },
  campaignAutomationNotFound: {
    code: 82,
    message: 'Chiến dịch tự động không tìm thấy'
	},
	feedNotFound: {
		code: 83,
		message: 'Bản tin không tìm thấy'
  },
  campaignManualNotFound: {
    code: 84,
    message: 'Chiến dịch không tìm thấy'
  },
  affiliateAccessTradeNotFound: {
    code: 89,
    message: 'Chương trình liên kết marketing AccessTrade không tìm thấy'
  },
  bankNotFound: {
    code: 86,
    message: 'Ngân hàng không tìm thấy'
  },
  mobileNetworkNotFound: {
    code: 87,
    message: 'Nhà mạng không tìm thấy'
  },
  onlineCategoryNotFound: {
    code: 88,
    message: 'onlineCategory không tìm thấy'
  },
  productNotFound: {
    code: 89,
    message: 'Sản phẩm không tìm thấy'
  },
  paymentTransactionNotFound: {
    code: 90,
    message: 'Giao dịch không tìm thấy'
  },
  codeNotFound: {
    code: 91,
    message: 'code không tìm thấy'
  },
  newsNotFound: {
    code: 92,
    message: 'news không tìm thấy'
  },
  newsPhotoNotFound: {
    code: 93,
    message: 'newsPhoto không tìm thấy'
  },
  commentNotFound: {
    code: 94,
    message: 'comment không tìm thấy'
  },
  errorCodeAndQuantity: {
    code: 95,
    message: 'Số lượng codes/qrcodes phải bằng số quà còn lại (quantity - gaveQuantity)'
  },
  errorQuantity: {
    code: 96,
    message: 'Số lượng quà phải >= quà đã trao'
  },
  plasticCardNotFound: {
    code: 97,
    message: 'Thẻ thành viên không tồn tại'
  },
  campaignAutomationV3NotFound: {
    code: 98,
    message: 'Chiến dịch tự động không tìm thấy'
  },
  cannotShareLuckyDraw: {
    code: 99,
    message: 'Bạn đã nhận thưởng share hôm nay rồi'
  }
}
