import User from './model'
import StringHelper from '../../util/stringHelper'
import commonCode from '../../locales/common'
import config from '../../config/env/index'
import TokenGenerator from '../../util/tokenGenerator'

const queries = {
  allUsers: async () => {
    return await User.find({})
  }
}

const mutations = {
  signIn: async (root, data) => {
    // const body = {
    //   name: data.name,
    //   email: data.authProvider.email.email,
    //   password: data.authProvider.email.password
    // }
    // return await User.create(body)
    let {phone, email, password} = data.email

    let isNew

    if (email) {
      email = email.toLowerCase()
    }

    phone = StringHelper.formatPhoneNumber(phone)

    let condition = []
    if (phone) {
      condition.push({phone})
    }
    if (email) {
      condition.push({email})
    }

    let user = await User.findOne({
      $or: condition
    })
    if (!user) {
      return commonCode.authenticateFailed
    }
    isNew = user.isNewUser
    if (process.env.NODE_ENV === config.env.production) {
      if (!user.authenticate(password)) {
        return commonCode.authenticateFailed
      }
    }

    user = await User.commonUserInfo(user)
    if (user.roles.indexOf(config.user.roles.sale) > -1) {
      user._id = global.adminId
      user.name = 'Zody - Hỗ trợ'
    }
    return {
      user,
      token: TokenGenerator.generate(user)
    }
  }
}

const data = {
  // User: {
  //   id: root => root._id || root.id,
  // }
}

export default {
  queries,
  mutations,
  data
}