export default {
	limit: {
		activity: 15, // Profile activity
		topCheckinPlaces: 3, // Profile: top checkin places
		search: 50, // Admin search
		reward: 10, // App reward history
		spin: 20, // App spin history
		chat: 15, // Recent chat history
		message: 20, // List messages
		zcoinHistory: 20, // For admin analytic
		voucherHistory: 20, // For admin analytic
		voucherUnused: 20 // For admin analytic
	},
	strict: {
		nameMinLength: 2,
		nameMaxLength: 64,
		pwdMinLength: 6,
		defaultAge: 20,
		age: {
			min: 10,
			max: 60
		}
	},
	roles: {
		authenticated: 'authenticated',
		admin: 'admin',
		business: 'business',
		customercare: 'customercare',
		sale: 'sale',
		staff: 'staff',
		chainManager: 'chainManager'
	},
	forgotPasswordTokenExpireTime: 86400000, // 1 day
	maxVerifySMS: 3 // per day
}
