/**
 * User helper functions
 */

import User from '../user/model'

/**
 * Check that new email address is already existed or not
 *
 * @param  {String} email: new user email
 * @param  {Function} callback
 * @return {Boolean}
 */
function checkUniqueEmail(email, callback) {
	// Find in db
	User.count({
		$and: [{
			email: {
				$exists: true
			}
		}, {
			email: email.toLowerCase()
		}]
	}, (error, c) => {
		callback(!c)
	})
}

function validateName(name) {
	let pattern = /^[a-zàáảãạăằắẳẵặâầấẩẫậèéẻẽẹêềếểễệòóỏõọôồốổỗộơờớởỡợùúủũụưừứửữựỳýỷỹỵìíỉĩịđ ,.'-]+$/i
	return new RegExp(pattern).test(name)
}

// Export
export default {
	checkUniqueEmail,
	validateName
}
