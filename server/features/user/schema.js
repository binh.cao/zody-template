const types = `
  type User {
    _id: ID!
    name: String!
    email: String!
    phone: String!
    city: String
    avatar: String
    birthday: String
    gender: String
  }
  
  type SignInPayload {
    token: String
    user: User
  }
  
  input AuthProviderSignupData {
    email: AUTH_PROVIDER_EMAIL
  }

  input AUTH_PROVIDER_EMAIL {
    email: String!
    password: String!
  }
`
const queries = `
  allUsers: [User!]!
`

const mutations = `
  signIn(email: AUTH_PROVIDER_EMAIL): SignInPayload!
`

export default {
  types,
  queries,
  mutations
}