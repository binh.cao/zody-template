import crypto from 'crypto'
import diacritics from 'diacritics'
import sanitizer from 'sanitizer'
import config from '../../config'
import {mongoose, Schema} from '../../util/mongoose'
import Helpers from './helper'
import UserConfig from "./config";
import Hooks from './hook'
import Statics from './static'
import userCode from '../../locales/user'
// import Methods from './method'
let removeDiacritics = diacritics.remove
let UserSchema = new Schema({
  name: {
    type: String,
    required: true,
    trim: true,
    minlength: [UserConfig.strict.nameMinLength],
    maxlength: [UserConfig.strict.nameMaxLength]
  },
  searchString: String,
  locale: {
    type: String,
    enum: config.locales.all.split(' '),
    default: config.locales.vi
  },
  city: {
    type: String,
    default: config.city.daNang
  },
  avatar: String,
  birthday: Date,
  phone: {
    type: String,
    required: true
  },
  email: {
    type: String,
    // unique: true,
    isEmail: {
      message: userCode.invalidEmailFormat.code.toString()
    },
    validate: [Helpers.checkUniqueEmail, userCode.emailAlreadyInUse.code.toString()]
  },
  gender: {
    type: String,
    default: config.gender.male
  },
  roles: {
    type: Array,
    default: [UserConfig.roles.authenticated]
  },
  facebook: {},
  hashed_password: String,
  resetPasswordToken: String,
  resetPasswordExpires: Date,
  salt: String,
  statistic: {
    checkin: {
      type: Number,
      default: 0
    },
    bill: {
      type: Number,
      default: 0
    },
    coin: {
      type: Number,
      default: 0
    },
    checkinCoin: {
      type: Number,
      default: 0
    },
    billedZcoin: {
      type: Number,
      default: 0
    },
    expense: {
      type: Number,
      default: 0
    },
    reward: {
      type: Number,
      default: 0
    },
    badge: {
      type: Number,
      default: 0
    },
    lastCheckinAt: Date,
    lastBillAt: Date
  },
  statuses: {
    online: {
      type: Boolean,
      default: true
    },
    verified: {
      type: Boolean,
      default: false
    },
    banned: {
      type: Boolean,
      default: false
    }
  },
  business: {
    type: Schema.ObjectId,
    ref: 'Business'
  },
  chain: {
    type: Schema.ObjectId,
    ref: 'Chain'
  },
  quests: [{
    type: Schema.ObjectId,
    ref: 'Quest'
  }],
  version: String,
  createdAt: {
    type: Date,
    default: Date.now
  },
  updatedAt: {
    type: Date,
    default: Date.now
  },
  lastActivedAt: Date,
  lastUpdatePasswordAt: Date,
  isNewUser: {
    type: Boolean,
    default: false
  },
  devideId: String
}, {
  versionKey: false
})

UserSchema.index({business: 1}).index({chain: 1}).index({quests: 1}).index({email: 1}).index({city: 1}).index({phone: 1}).index({roles: 1}).index({searchString: 1})

/**
 * Virtual
 */
UserSchema.virtual('password').set(function (password) {
  this._password = password
  this.salt = this.makeSalt()
  this.hashed_password = this.hashPassword(password)
}).get(function () {
  return this._password
})

/**
 * Methods
 */
UserSchema.methods = {
  /**
   * Authenticate - check if the passwords are the same
   *
   * @apiIgnore
   * @param {String} plainText
   * @return {Boolean}
   * @api public
   */
  authenticate: function (plainText) {
    return this.hashPassword(plainText) === this.hashed_password
  },
  /**
   * Make salt
   *
   * @apiIgnore
   * @return {String}
   * @api public
   */
  makeSalt: function () {
    return crypto.randomBytes(16).toString('base64')
  },

  /**
   * Hash password
   *
   * @apiIgnore
   * @param {String} password
   * @return {String}
   * @api public
   */
  hashPassword: function (password) {
    if (!password || !this.salt) return ''
    var salt = new Buffer(this.salt, 'base64')
    return crypto.pbkdf2Sync(password, salt, 10000, 64, 'sha1').toString('base64')
  }
}

UserSchema.statics = Statics
// UserSchema.methods = Object.assign(UserSchema.methods, Methods)

UserSchema.pre('save', function(next) {
  if (this.isNew) {
    this._isNew = true
  }

  // Sanitizer
  this.name = sanitizer.sanitize(this.name)

  // Set search string
  this.searchString = removeDiacritics(this.name).toLowerCase()

  next()
})

UserSchema.post('save', function(doc) {
  Hooks.postSaveHook(doc)
})

// Export
export default mongoose.model('User', UserSchema)
