import config from '../../config'
import moment from 'moment'
import util from '../../util'
import logger from "../../util/app-logger";

function buildMatchForIdsByCondition(condition) {
	let match = {}
	let exceptIds = condition.except
	if(exceptIds && Array.isArray(exceptIds) && exceptIds.length > 0) {
		match._id = {
			$nin: exceptIds
		}
	}
	if(condition.hasKey('userName')) {
		match.searchString = util.prepareSearchString(condition.userName)
	}
	const allGender = config.gender.list.split(' ')[0]
	if(condition.hasKey('gender') && condition.gender !== allGender) {
		match.gender = {
			$in: [condition.gender, allGender]
		}
	}
	const allCity = config.city.list.split(' ')[0]
	if(condition.hasKey('city') && condition.city !== allCity) {
		match.city = {
			$in: [condition.city, allCity]
		}
	}
	if(condition.hasKey('birthday')) {
		let birthday = condition.birthday
		match.birthday = {
			$gte: new Date(moment(birthday).startOf('day').toISOString()),
			$lte: new Date(moment(birthday).endOf('day').toISOString())
		}
	} else if(condition.hasKey('fromAge') || condition.hasKey('toAge')){
		match.birthday = {}
		if(condition.hasKey('fromAge')) {
			match.birthday['$lte'] = new Date(moment().subtract(condition.fromAge, 'year').endOf('year'))
		}
		if(condition.hasKey('toAge')) {
			match.birthday['$gte'] = new Date(moment().subtract(condition.toAge, 'year').startOf('year'))
		}
	}
	// logger.logToJson(match)
	return match
}

function buildIdsByCondition(condition) {
	return [
		{
			$match: buildMatchForIdsByCondition(condition)
		},
		{
			$project: {
				_id: 1
			}
		}
	]
}

export default {
	buildIdsByCondition
}
