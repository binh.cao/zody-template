import config from '../../config'
// import {Referral} from '../referral'
import lodash from 'lodash'
import util from '../../util'
import {ObjectId} from '../../util/mongoose'
// import UserRepository from './repository'
import User from './model'
import PhotoHelper from '../../util/photoHelper'

async function commonUserInfo(data) {
	if (!data.city) {
		data.city = config.city.daNang
	}
	let results = {
		avatar: avatar(data),
		city: data.city,
		gender: data.gender || config.gender.male,
		phone: data.phone
	}
	if (data.facebook && data.facebook.profileUrl) {
		results.facebook = data.facebook.profileUrl
	} else {
		results.facebook = config.conventions.string
	}
	data = lodash.pick(data, ['_id', 'name', 'verified', 'statistic', 'statuses', 'birthday', 'roles', 'locale', 'business', 'chain', 'version', 'lastUpdatePasswordAt'])
	return Object.assign(data, results)
}

async function commonUserInfoById(userId) {
	try {
		let user = await User.findOne({
			_id: new ObjectId(userId)
		}).lean().exec()
		return await commonUserInfo(user)
	} catch (error) {
		return PhotoHelper.defaultUser()
	}
}

async function briefInfo(data) {
	let results = {}
	results.avatar = avatar(data)
	data = lodash.pick(data, ['_id', 'name', 'roles', 'business', 'locale', 'gender', 'city', 'integratedId', 'phone'])
	return Object.assign(data, results)
}

async function getById(userId) {
	try {
		let user = await User.findOne({
			_id: new ObjectId(userId)
		}).lean().exec()
		return await briefInfo(user)
	} catch (error) {
		return PhotoHelper.defaultUser()
	}
}

/**
 * Get user avatar, priority: local > facebook > no avatar
 *
 * @param  {Object} data: user data
 * @return {String}
 */
function avatar(data) {
	// If has app avatar, use it
	if (data.avatar) {
		return PhotoHelper.getAvatarUrl(data.avatar)
	}
	// Else find for facebook avatar
	else if (data && data.facebook && data.facebook.photos) {
		return data.facebook.photos
	}
	// Else use default
	else {
		return PhotoHelper.defaultAvatar()
	}
}

export default {
	commonUserInfo
}
