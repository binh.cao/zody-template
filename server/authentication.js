const HEADER_REGEX = /bearer token-(.*)$/i
import User from './features/user/model'

module.exports.authenticate = async ({headers: {authorization}}) => {
  const email = authorization && HEADER_REGEX.exec(authorization)[1]
  return email && await User.findOne({email})
}