import config from './config'
import {mongoose} from './util/mongoose'
mongoose.set('debug', true)

async function connect() {
  mongoose.connect(config.db.fullPath, config.db.options(), (error) => {
    if (error) {
      console.log('Error on connecting to the database: ', error)
      console.log('\x1b[31m', '*** PLEASE CONNECT TO DATABASE BEFORE RUN SERVER', '\x1b[0m')
      process.exit(1)
    } else {
      console.log('db connected')
    }
  })
}
export default {
  connect
}