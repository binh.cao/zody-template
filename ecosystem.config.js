/*jshint camelcase: false */
let appName = 'Misena'
let ipServer1 = '128.199.219.104'
let ipServer2 = '128.199.113.110'
let ipServer3 = '128.199.121.238'
let ipServerDev = '128.199.219.104'
let user = 'misena'
let sshPort = '2323'
let productionBranch = 'origin/master'
let developmentBranch = 'origin/develop'
let repo = 'git@gitlab.com:binh.cao/misena.git'
let path = '/home/misena/misena_api'
let postDeployProduction = 'ln -nfs ../shared/.env .env && \
                      ln -nfs ../shared/credential.json credential.json && \
                      npm install --production && \
                      pm2 startOrRestart ecosystem.config.js --env production'
let postDeployDev = 'ln -nfs ../shared/.env .env && \
                      ln -nfs ../shared/credential.json credential.json && \
                      npm install && \
                      npm run apidoc && \
                      pm2 startOrRestart ecosystem.config.js --env dev && \
                      source /home/misena/kill3000.sh'

module.exports = {
  apps: [{
    name: appName,
    script: './.start.sh',
    env_production: {
      NODE_ENV: 'production'
    },
    env_dev: {
      NODE_ENV: 'development'
    }
  }],

  deploy: {
    production: {
      user: user,
      host: [{
        host: ipServer1,
        port: sshPort
      }, {
        host: ipServer2,
        port: sshPort
      }, {
        host: ipServer3,
        port: sshPort
      }],
      ref: productionBranch,
      repo: repo,
      path: path,
      'post-deploy': postDeployProduction
    },
    app1: {
      user: user,
      host: [{
        host: ipServer1,
        port: sshPort
      }],
      ref: productionBranch,
      repo: repo,
      path: path,
      'post-deploy': postDeployProduction
    },
    app2and3: {
      user: user,
      host: [{
        host: ipServer2,
        port: sshPort
      }, {
        host: ipServer3,
        port: sshPort
      }],
      ref: productionBranch,
      repo: repo,
      path: path,
      'post-deploy': postDeployProduction
    },
    dev: {
      user: user,
      host: {
        host: ipServerDev,
        port: sshPort
      },
      ref: developmentBranch,
      repo: repo,
      path: path,
      'post-deploy': postDeployDev
    }
  }
}